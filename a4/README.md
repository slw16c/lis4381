> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Architecture

## Steve Walter

### Assignment 4 Requirements:

*Five Parts:*

1. Clone assignment files
2. Modify files according to directions
3. Add code to jQuery
4. Create favicon
5. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A4
* Screenshot of main page
* Screenshot of Failed Validation
* Screenshot of Passed Validation

#### Assignment Screenshots:

*Screenshot of Main Page*:

![LIS 4381 Main Page](img/a4img1.png)

*Screenshot of Failed Validation*:

![Failed Validation](img/a4img2.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/a4img3.png)