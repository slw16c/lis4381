# LIS4331: ADVANCED MOBILE APPLICATION DEVELOPMENT

## Steve Walter

### Assignment 1 Requirements:

*four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Bitbucket repo links: a) This assignment and b) The completed tutorials above.
4. Questions

#### README.md file should include the following items:

* Screenshot of running JDK java Hello
* Screenshot of running Android Studio - My First App
* Screenshot of running Android Studio - Contacts App
* Git commands with short descriptions
* Bitbucket repo links

#### Git commands w/short descriptions:

1. git init - Creates a new local repository
2. git status - Lists the files you've changed and those you still need to add or commit
3. git add - Adds one or more files to staging
4. git commit - Commits the changes to the head
5. git push - Sends the changes to the master branch of remote repository
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git clone - Creates a working of a local repository

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javass.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/myfirstapp.jpg)

##### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/slw16c/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")