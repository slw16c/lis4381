<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio">
		<meta name="author" content="Steve Walter">
    <link rel="icon" type="image/png" href="icon.png">

		<title>LIS 4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>	

		<style type="text/css">

			body
			{
    		background-color: #ccccb3
			}
			.text-justify 
			{
				font-size: 20px
			}
			h4
			{
				font-size: 20px
			}
		</style>

  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I set up a distributed version control with BitBucket. I installed Android Studio and created my first app. Lastly, i made sure my Java was up-to-date for compiling and running java programs.
				</p>

&nbsp;

				<h4>Java Installation</h4>
				<img src="img/javass.png" class="img-responsive center-block" alt="JDK Installation">
				&nbsp;
				<h4>Android Studio Installation</h4>
				<img src="img/myfirstapp.jpg" class="img-responsive center-block" alt="Android Studio Installation">
				&nbsp;
				<h4>AMPPS Installation</h4>
				<img src="img/ampps.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
