> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Architecture

## Steve Walter

### Assignment 3 Requirements:

*Three Parts:*

1. Create a database and an EDR. Fill it with data
2. Create a mobile app that calculates ticket prices
3. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A3
* Screenshot of ERD
* Screenshot of running application’s first user interface
* Screenshot of running application's second user interface
* Link to a3.mwb file
* Link to a3.sql file

#### Assignment Screenshots:

![Assignment 3 ERD](img/a3.png)

*Links for Assignment 3*:

*Screenshot of Android Studio activity 1*:

![Android Studio User Interface 1 Screenshot](img/a3img1.jpg)

*Screenshot of Android Studio activity 2*:

![Android Studio User Interface 2 Screenshot](img/a3img2.jpg)

*Screenshot of Assignment 3 ERD*:


[Link for a3.mwb](links/a3.mwb "Link for a3.mwb")

[Link for a3.sql](links/a3.sql "Link for a3.sql")