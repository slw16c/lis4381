<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio">
		<meta name="author" content="Steve Walter">
		<link rel="icon" type="image/png" href="icon.png">

		<title>LIS 4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>		

		
		<style type="text/css">

			body
			{
    		background-color: #ccccb3
			}
			.text-justify 
			{
				font-size: 20px
			}
			h4
			{
				font-size: 20px
			}
			p
			{
				font-size: 25px
			}
		</style>


  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>		
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I was tasked to create a database in MySQL. The database would include 10 inserts per table. also, included with the database, an ERD showing the relationships. I was also tasked to create an app that calculated ticket prices.
				</p>
				&nbsp;
				<h4>Link for Database .mwb file</h4>
				<p><a href="links/a3.mwb">Assignment 3 MWB file</a></p>
				&nbsp;
				<h4>Link for Database .sql file</h4>
				<p><a href="links/a3.sql">Assignment 3 SQL file</a></p>
				&nbsp;
				<h4>ERD for the database</h4>
				<img src="img/a3.png" class="img-responsive center-block" alt="ERD for the database">
				&nbsp;
				<h4>Activity 1 for concert app</h4>
				<img src="img/a3img1.jpg" class="img-responsive center-block" alt="Activity 1 for concert app">
				&nbsp;
				<h4>Activity 2 for concert app</h4>
				<img src="img/a3img2.jpg" class="img-responsive center-block" alt="Activity 2 for concert app">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
