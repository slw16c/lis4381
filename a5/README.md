> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Architecture

## Steve Walter

### Assignment 5 Requirements:

*Five Parts:*

1. Obtiain A4 cloned files
2. Review new A5 directory
3. Change a4 index.php to add_petstore.php
4. edite code in php files to run the database and form validation
5. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A5
* Screenshot of working databse table
* Screenshot of Failed Validation

#### Assignment 5 Screenshots:

*Screenshot of Database Table*:

![Working Database Table](img/a5p1.png)

*Screenshot of Failed Validation*:

![Failed Validation](img/a5p2.png)