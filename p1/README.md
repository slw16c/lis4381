> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Architecture

## Steve Walter

### Project 1 Requirements:

*Two Parts:*

1. Create a Business Card App
2. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per P1
* Screenshot of running application’s first user interface
* Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of Android Studio activity 1*:

![Android Studio User Interface 1 Screenshot](img/p1img1.jpg)

*Screenshot of Android Studio activity 2*:

![Android Studio User Interface 2 Screenshot](img/p1img2.jpg)