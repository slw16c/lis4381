<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio">
		<meta name="author" content="Steve Walter">
    <link rel="icon" type="image/png" href="icon.png">

		<title>LIS 4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>		

		
		<style type="text/css">

			body
			{
    		background-color: #ccccb3
			}
			.text-justify 
			{
				font-size: 20px
			}
			h4
			{
				font-size: 20px
			}
		</style>


  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I was tasked to set create a Business Card App with two activities. The app shows my name, picture and button in the first activity. The second activity has contact information and shows my interests. 
				</p>
				&nbsp;
				<h4>Activity 1</h4>
				<img src="img/p1img1.jpg" class="img-responsive center-block" alt="Activity One">
				&nbsp;
				<h4>Activity 2</h4>
				<img src="img/p1img1.jpg" class="img-responsive center-block" alt="Activity Two">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
