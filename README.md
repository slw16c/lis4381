




# LIS4381 Mobile Web Application Architecture

## Steve Walter

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    * Install .NET Core
    * Create hwapp application
    * Create aspnetcoreapp application
    * Provide screenshots of installations
    * Create Birbucket Repo
    * Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    * Starting the creation of an app using Android Studio
    * Implementing widgets via the design tab of .xml file
    * Creating an id for each item in the app
    * Setting up a second activity
    * Writing code to allow the button in the app to go from Activity 1 to Activity 2

3. [A3 README.md](a3/README.md "My A3 README.md file")

    * Creating a local connection for mySQL
    * Sketching out database per business rules
    * Creating ERD for database and filling it with data
    * Foward-Engineering the ERD
    * Creating a mobile app that calculates ticket prices
    * Adding a background to the mobile app

4. [A4 README.md](a4/README.md "My A4 README.md file")

    * cd to Local Repo
    * Clone the assignment starter files
    * Review the subdirectories
    * Modify meta tags
    * Change tile, and headers
    * Add jOuery code to make the validations work
    * Create a favicon

5. [A5 README.md](a5/README.md "My A5 README.md file")

    * Made a web application that displays a databse
    * Connected Ampps with Mysql Localhost
    * Turned off client-side validation
    * Added error pages for server-side validations
    * Made the 'Add Petstore' page allow inserted data
    * Added delete and edit buttons to the Database table

6. [P1 README.md](p1/README.md "My P1 README.md file")

    * Create Business Card App by backward-engineering
    * Creating a launcher icon
    * Adding background colors to both activities
    * Adding boarder around image and button
    * Adding text shadow to the button

7. [P2 README.md](p2/README.md "My P2 README.md file")

    * Edited the A5 web application
    * Made the Edit button work
    * Made the delete button work
    * Added an RSS feed