> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Architecture

## Steve Walter

### Assignment 2 Requirements:

*Two Parts:*

1. Create a mobile recipe app using Android Studio
2. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A2
* Screenshot of running application’s first activity
* Screenshot of running application’s second avtivity

#### Assignment Screenshots:

*Screenshot of Android Studio activity 1*:

![Android Studio Activity 1 Screenshot](img/activity1.jpg)

*Screenshot of Android Studio activity 2*:

![Android Studio Activity 2 Screenshot](img/activity2.jpg)