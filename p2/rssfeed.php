<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Using RSS Feeds</title>
    <link rel="icon" type="image/png" href="icon.png">
	<?php include_once("../css/include_css.php"); ?>


	<style type="text/css">

		body
		{
			background-color: #ccccb3;
		}
		.text-justify 
		{
			font-size: 30px;
		}
        h2
        {
            font-size: 30px;
        }
		h4
		{
			font-size: 20px;
            text-decoration: underline;
		}
        p
        {
            font-size: 20px;
        }
	</style>
</head>

<body>
<?php include_once("../global/nav.php"); ?>
    <?php

    $url = 'http://www.techradar.com/rss';
    $feed = simplexml_load_file($url, 'SimpleXMLIterator');
    echo "<h2>" . $feed->channel->description . "</h2><ol>";

    $filtered = new LimitIterator($feed->channel->item, 0, 10);
    foreach ($filtered as $item) {?>
        <h4><li><a href="<?= $item->link; ?>" target="_blank"><?= $item->title;?></a></li></h4>
        <?php
            date_default_timezone_set('America/New_York');

            $date = new DateTime($item->pubDate);
            $date->setTimezone(new DateTimeZone('America/New_York'));
            $offset = $date->getOffset();
            $timezone = ($offset == -14400) ? ' ETD' : ' EST';

            echo $date->format('M j, Y, g:ia') . $timezone;
        ?>

            <p><?php echo $item->description; ?></p>
    <?php } ?>
</ol>
</body>
</html>