> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Architecture

## Steve Walter

### Project 2 Requirements:

*Six Parts:*

1. Clone A5 files
2. Rename to edit_petstore
3. Create php file to make 'edit' button work
4. Create php file to make 'delete' button work
5. Add an RSS Feed
6. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A5
* Screenshot of working databse table
* Screenshot of working edit_petstore.php
* Screenshot of failed validation
* Screenshot of Home Page Carousel
* Screenshot of RSS Feed

#### Project 2 Screenshots:

*Screenshot of Database Table*:

![Working Database Table](img/p2img1.png)

*Screenshot of Edit Petstore*:

![Working Edit Petstore](img/p2img2.png)

*Screenshot of Failed Validation*:

![Failed Validation](img/p2img3.png)

*Screenshot of Homepage Carousel*:

![Working Homepage Carousel](img/p2img4.png)

*Screenshot of RSS Feed*:

![Working RSS Feed](img/p2img5.png)
